package vignette

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestFileNameIsChanged(t *testing.T) {
	svg, rb := prep("testRes/A4-Portrait-0.svg", 0)
	newSVGGenerator().Generate(svg)

	err := verify("testRes/reference0.svg", rb, t)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestOneLineIsChanged(t *testing.T) {
	svg, rb := prep("testRes/A4-Portrait-1.svg", 1)
	newSVGGenerator().Generate(svg)

	err := verify("testRes/reference1.svg", rb, t)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestAllLinesAreChanged(t *testing.T) {
	svg, rb := prep("testRes/A4-Portrait-6.svg", 6)
	newSVGGenerator().Generate(svg)

	err := verify("testRes/reference6.svg", rb, t)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestPassesEvenWithLessSinglesThanInTemplate(t *testing.T) {
	svg, rb := prep("testRes/A4-Portrait-6.svg", 5)
	newSVGGenerator().Generate(svg)

	if err := verify("testRes/reference5.svg", rb, t); err != nil {
		t.Errorf("%v", err)
	}
}

func prep(templateFile string, singlesPerTemplate int) (*SVGConfig, *bytes.Buffer) {
	tf, _ := os.Open(templateFile)
	resultBuffer := bytes.NewBufferString("")

	svg := &SVGConfig{
		pageWriter: resultBuffer,
		singles:    prepSingles(singlesPerTemplate),
		SVGTemplateConfig: SVGTemplateConfig{
			SinglesPerTemplate: singlesPerTemplate,
			SVGTemplate:        tf,
			BackFileName:       "test.jpg",
		}}
	svg.pageWritterDefer = func() {}
	return svg, resultBuffer
}

func verify(referenceFile string, buff *bytes.Buffer, t *testing.T) error {
	ref, err := ioutil.ReadFile(referenceFile)
	if err != nil {
		return nil
	}

	actual := buff.String()
	if actual != string(ref) {
		t.Logf("%s\n%s", t.Name(), actual)
		return fmt.Errorf("does not match reference %s", referenceFile)
	}

	return nil
}

func prepSingles(c int) []*single {
	ret := make([]*single, c)
	for i := 0; i < c; i++ {
		ret[i] = &single{"testL1", "testL2"}
	}

	return ret
}
