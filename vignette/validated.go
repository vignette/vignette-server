package vignette

import (
	"errors"
)

type Validated interface {
	Validate() error
}

func (v *VignetteConfig) validate() error {
	// TODO specify messages and test
	if v == nil {
		return errors.New("")
	}
	if v.SinglesConfigFilePath == "" {
		return errors.New("")
	}
	// if _, err := os.Stat(v.SinglesConfigFilePath); os.IsNotExist(err)  TODO
	if v.SVGTemplateConfig == nil {
		return errors.New("Missing SVGConfig")
	}

	return v.SVGTemplateConfig.validate()
}

func (s *SVGTemplateConfig) validate() error {
	if s.SVGTemplate == nil {
		return errors.New("Missing SVGConfig reader")
	}
	if s.SinglesPerTemplate == 0 {
		return errors.New("Missing SVGConfig singles")
	}
	if s.BackFileName == "" {
		// return fmt.Errorf("File \"%s\" does not exist", s.BackFileName)
	}

	return nil
}
