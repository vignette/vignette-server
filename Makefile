# .PHONY

all: deps test run
# docker: deps verify compile-linux docker-build docker-push

PROJECT_NAME=vignette-server
PROJECT_REPOSITORY=gitlab.com/kskitek/$(PROJECT_NAME)
DOCKER_REGISTRY=registry.$(PROJECT_REPOSITORY)

deps:
	dep ensure

test:
	go test -race ./...

verify:
	go fmt ./...
	go vet ./...
	go test ./... -race

compile: test
	go build

run: compile
	./$(PROJECT_NAME)

compile-linux: test
	env GOOS=linux go build -o $(PROJECT_NAME)-linux

dbuild: compile-linux
	docker build -t $(DOCKER_REGISTRY) .

drun: dbuild
	docker run --rm -it -p 8080:8080 $(DOCKER_REGISTRY)

# TODO docker-push