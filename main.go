package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/vignette/vignette-server/vignette"

	"github.com/ajstarks/svgo"
)

func main() {
	f, _ := os.Open("resources/template.svg")
	svgConf := &vignette.SVGTemplateConfig{SinglesPerTemplate: 6, SVGTemplate: f}
	vc := &vignette.VignetteConfig{SinglesConfigFilePath: "vignette/testRes/9lines.csv", SVGTemplateConfig: svgConf}
	vignette.New().Start(vc)

	http.HandleFunc("/circle", circle)
	// TODO static
	http.HandleFunc("/healthz", healthz)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
		os.Exit(1)
	}
}

func static(w http.ResponseWriter, req *http.Request) {
	dat, _ := ioutil.ReadFile(req.URL.Path)
	w.Write(dat)
	w.WriteHeader(http.StatusOK)
}

func healthz(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func back(w http.ResponseWriter, req *http.Request) {
	dat, _ := ioutil.ReadFile("back1.jpg")
	w.Write(dat)
}
func circle(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	s := svg.New(w)
	s.Start(1000, 1000)
	s.Def()
	s.Pattern("single", 0, 0, 1, 1, "objectBoundingBox", "viewBox=\"0 0 100 100\"")
	s.Image(0, 0, 100, 100, "back1.jpg", "preserveAspectRatio=\"none\"")
	s.PatternEnd()
	s.DefEnd()

	s.Circle(110, 110, 100, "fill:url(#single);stroke:#000000")
	s.Circle(330, 110, 100, "fill:url(#single);stroke:#000000")
	s.Rect(300, 300, 500, 500, "fill:url(#single);stroke:#999999")
	s.End()
}
