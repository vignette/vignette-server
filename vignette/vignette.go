package vignette

import (
	"fmt"
	"os"
)

type Vignette interface {
	Start(*VignetteConfig) error
}

type VignetteConfig struct {
	SinglesConfigFilePath string
	SVGTemplateConfig     *SVGTemplateConfig
}

type vignetteS struct {
	configReader configReader
	svgGenerator SVGGenerator
}

func New() Vignette {
	return &vignetteS{
		configReader: newCSVConfigReader(),
		svgGenerator: newSVGGenerator(),
	}
}

func (v *vignetteS) Start(conf *VignetteConfig) error {
	verr := conf.validate()
	if verr != nil {
		return verr
	}

	singles, cerr := v.configReader.Read(conf.SinglesConfigFilePath)
	if cerr != nil {
		return cerr
	}

	pagesCount := len(singles) / conf.SVGTemplateConfig.SinglesPerTemplate
	generatePages(v.svgGenerator, conf.SVGTemplateConfig, singles, pagesCount)

	return nil
}

func generatePages(g SVGGenerator, svgConf *SVGTemplateConfig, singles []*single, pagesCount int) error {
	for pageIdx := 1; pageIdx <= pagesCount; pageIdx++ {
		idxLow := (pageIdx - 1) * svgConf.SinglesPerTemplate
		idxHigh := idxLow + svgConf.SinglesPerTemplate

		conf, err := prepareConfig(svgConf, singles[idxLow:idxHigh], pageIdx)
		if err != nil {
			return err
		}
		g.Generate(conf)
	}

	idxLow := pagesCount * svgConf.SinglesPerTemplate
	rest := len(singles) % idxLow
	if rest != 0 {
		conf, err := prepareConfig(svgConf, singles[idxLow:], pagesCount+1)
		if err != nil {
			return err
		}
		g.Generate(conf)
	}

	return nil
}

func prepareConfig(t *SVGTemplateConfig, singles []*single, pageIndex int) (*SVGConfig, error) {
	retConf := &SVGConfig{SVGTemplateConfig: SVGTemplateConfig{
		SVGTemplate:        t.SVGTemplate,
		SinglesPerTemplate: t.SinglesPerTemplate,
	}}

	f, ferr := prepareResultFile(pageIndex)
	if ferr != nil {
		return nil, ferr
	}

	updateConfig(retConf, singles, pageIndex, f)
	return retConf, nil
}

func prepareResultFile(pageIndex int) (*os.File, error) {
	// TODO maybe provide here sth like WritterFactory so anyone can change it. It is needed?
	fileName := fmt.Sprintf("%d.svg", pageIndex)
	return os.Create(fileName)
}

func updateConfig(conf *SVGConfig, singles []*single, pageIndex int, f *os.File) {
	conf.singles = singles
	conf.pageIndex = pageIndex
	conf.pageWriter = f
	conf.pageWritterDefer = func() { f.Close() }
}
