package vignette

import (
	"encoding/csv"
	"os"
)

type single struct {
	line1 string
	line2 string
}

type configReader interface {
	Read(CSVFilePath string) ([]*single, error)
}

type csvConfig struct {
}

func (conf *csvConfig) Read(CSVFilePath string) ([]*single, error) {
	file, ferr := os.Open(CSVFilePath)
	if ferr != nil {
		return nil, ferr
	}

	r := csv.NewReader(file)
	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}

	singles := recordsToSingles(records)

	return singles, nil
}

func newCSVConfigReader() configReader {
	return &csvConfig{}
}

func recordsToSingles(records [][]string) []*single {
	singles := make([]*single, len(records))
	singles[0] = &single{}

	for idx, line := range records {
		if len(line) == 2 {
			singles[idx] = &single{line[0], line[1]}
		}
	}
	return singles
}
