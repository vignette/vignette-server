package vignette

import (
	"errors"
	"strings"
	"testing"
)

var mockSVG = &MockSVGGenerator{}

func vng(vc *VignetteConfig, mockedSinglesCount int) error {
	mockSVG.calledWithLen = make([]int, 100)
	mockSVG.timesCalled = 0

	v := &vignetteS{
		configReader: &mockConfigReader{mockedSinglesCount},
		svgGenerator: mockSVG,
	}
	return v.Start(vc)
}

func TestErrorWhenSVGConfigIsNil(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig = nil
	err := vng(vc, 1)

	expected(t, errors.New("Missing SVGConfig"), err)
}

func TestErrorWhenSVGConfigPathIsEmpty(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig.SVGTemplate = nil
	err := vng(vc, 1)

	expected(t, errors.New("Missing SVGConfig reader"), err)
}

func TestErrorWhenSVGConfigSinglesIsZero(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig.SinglesPerTemplate = 0
	err := vng(vc, 1)

	expected(t, errors.New("Missing SVGConfig singles"), err)
}

func TestExitsWhenReadingConfigFailed(t *testing.T) {
	vc := okConfig()
	vc.SinglesConfigFilePath = "doesNotExist.txt"
	err := vng(vc, 1)

	expectedNot(t, err, nil)
}

func TestFromFailingCase(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig.SinglesPerTemplate = 6
	vng(vc, 9)

	expected(t, 2, mockSVG.timesCalled)
	expected(t, 6, mockSVG.calledWithLen[0])
	expected(t, 3, mockSVG.calledWithLen[1])
}

func TestPassesOnlyXSinglesPerTemplate(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig.SinglesPerTemplate = 2
	vng(vc, 9)

	expected(t, 5, mockSVG.timesCalled)
	expected(t, 2, mockSVG.calledWithLen[0])
	expected(t, 1, mockSVG.calledWithLen[4])
}

func TestGenerates1PageWhenSinglesMatchesSinglesPerTemplate(t *testing.T) {
	vc := okConfig()
	vc.SVGTemplateConfig.SinglesPerTemplate = 9
	vng(vc, 9)

	expected(t, 1, mockSVG.timesCalled)
}

type MockSVGGenerator struct {
	timesCalled   int
	calledWithLen []int
}

func (m *MockSVGGenerator) Generate(c *SVGConfig) error {
	m.calledWithLen[m.timesCalled] = len(c.singles)
	m.timesCalled++
	return nil
}

type mockConfigReader struct {
	howManySingles int
}

func (m *mockConfigReader) Read(CSVFilePath string) ([]*single, error) {
	if CSVFilePath == "doesNotExist.txt" {
		return nil, errors.New("file does not exist...")
	}

	ret := make([]*single, m.howManySingles)
	s := &single{line1: "1", line2: "2"}
	for i := 0; i < m.howManySingles; i++ {
		ret[i] = s
	}

	return ret, nil
}

func okConfig() *VignetteConfig {
	return &VignetteConfig{
		SinglesConfigFilePath: "somePath",
		SVGTemplateConfig:     okTemplateConfig(),
	}
}

func okTemplateConfig() *SVGTemplateConfig {
	return &SVGTemplateConfig{
		SinglesPerTemplate: 1,
		SVGTemplate:        strings.NewReader(""),
	}
}
