FROM alpine:3.7
EXPOSE 8080
COPY vignette-server-linux /vignette-server
ENTRYPOINT [ "/vignette-server" ]
