package vignette

import (
	"testing"
)

func read(filePath string) ([]*single, error) {
	cr := newCSVConfigReader()
	return cr.Read("testRes/" + filePath)
}

func TestReadingFailsWhenFileWasNotFound(t *testing.T) {
	_, err := read("there is not such file.txt")
	if err == nil {
		t.Error("It has to fail for missing file!")
	}
}

func TestReadingOneLine(t *testing.T) {
	singles, _ := read("oneLine.csv")

	expected(t, 1, len(singles))
	expected(t, "TestLine1", singles[0].line1)
	expected(t, "TestLine2", singles[0].line2)
}

func TestReadingIgnoresEmptyLines(t *testing.T) {
	singles, _ := read("emptyLines.csv")

	expected(t, 2, len(singles))
}
