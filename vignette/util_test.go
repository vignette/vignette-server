package vignette

import "testing"

func expected(t *testing.T, exp interface{}, act interface{}) {
	if exp != act {
		eErr, ok := exp.(error)
		if ok {
			aErr, _ := act.(error)
			expected(t, eErr.Error(), aErr.Error())
		} else {
			t.Errorf("Expected \"%v\" got \"%v\"", exp, act)
		}
	}
}

func expectedNot(t *testing.T, exp interface{}, act interface{}) {
	if exp == act {
		t.Errorf("Expected not \"%v\"", exp)
	}
}
