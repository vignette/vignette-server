package vignette

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

type SVGGenerator interface {
	Generate(*SVGConfig) error
}

type SVGTemplateConfig struct {
	SVGTemplate        io.Reader
	SinglesPerTemplate int
	BackFileName       string
}

type SVGConfig struct {
	singles          []*single
	pageIndex        int
	pageWriter       io.Writer
	pageWritterDefer func()
	SVGTemplateConfig
}

type generator struct {
}

func newSVGGenerator() SVGGenerator {
	return &generator{}
}

func (g *generator) Generate(c *SVGConfig) error {
	defer c.pageWritterDefer()
	s := bufio.NewScanner(c.SVGTemplate)

	replacedFile := false
	sIdx := 0
	for s.Scan() {
		text := s.Text()
		newLine := text

		if !replacedFile {
			newLine, replacedFile = replaceFileName(text, c.BackFileName)
		} else {
			s := getSingleForIdx(c.singles, sIdx)
			newLine, sIdx = replaceWithSingle(text, s, sIdx)
		}

		fmt.Fprintf(c.pageWriter, "%s\n", newLine)
	}

	return nil
}

func replaceFileName(t string, f string) (string, bool) {
	if i := strings.Index(t, "$imagePath"); i > 0 {
		return strings.Replace(t, "$imagePath", f, 1), true
	}
	return t, false
}

func replaceWithSingle(t string, s *single, sIdx int) (string, int) {
	if i := strings.Index(t, "$Line1"); i > 0 {
		return strings.Replace(t, "$Line1", s.line1, 1), sIdx
	}
	if i := strings.Index(t, "$Line2"); i > 0 {
		newLine := strings.Replace(t, "$Line2", s.line2, 1)
		sIdx++
		return newLine, sIdx
	}

	return t, sIdx
}

var emptySingle = &single{"", ""}

func getSingleForIdx(singles []*single, sIdx int) *single {
	if sIdx > len(singles)-1 {
		return emptySingle
	}
	return singles[sIdx]
}
